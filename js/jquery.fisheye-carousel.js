/**
 * Created by haloowing on 11/5/15 AD.
 */
(function ($) {

    $.fn.fisheye = function (options, values) {

        // init
        if (options === undefined || typeof options === 'object') {
            var settings = $.extend({}, $.fn.fisheye.defaults, options);

            return this.each(function () {
                
                $(this).addClass('fisheye');
                
                var instance = new Fisheye($(this), settings);
                
                $(this).data('context', instance);
            });
        } else if (options === 'instance') {
            return this.data('context');
        }
    };

    $.fn.fisheye.defaults = {
        /* default options */
        ratio : 0.5,
        interval : 5000
    };
    
    var Fisheye = function($element, options) {
        this.$element = $element;
        this.settings = options;
        this.config = {};
        this.slideTimer = null;
        
        this.init();
        this.render();
        
        this.start();
    };
    
    Fisheye.prototype.init = function() {
        var o = this;
        
        this.config.itemCount = this.$element.find('.item').length;
        this.config.activeItemScale = 1.2;
        
        this.$element.find('.next').after('<div style="clear:both;"></div>');
        
        var counter = 0;
        this.$element.find('.item').each(function() {
            $(this).attr('data-index', counter);
            counter++;
        });
        
        $(window).resize(function() {
            if(o.config.timer) {
                clearTimeout(o.config.timer);
                o.config.timer = null;
            }
            
            o.config.timer = setTimeout(function() {
                o.render();
            }, 400);
        });
        
        this.$element.find('.prev').click(function() {
            var activeIndex = parseInt(o.$element.find('.item.active').attr('data-index'));
            o.stop();
            o.navigate(activeIndex - 1);
            o.start();
        });
        this.$element.find('.next').click(function() {
            var activeIndex = parseInt(o.$element.find('.item.active').attr('data-index'));
            o.stop();
            o.navigate(activeIndex + 1);
            o.start();
        });
        
        // dot
        for(var i = 0; i < this.config.itemCount; i++) {
            this.$element.find('.bottompart').append('<div class="dot"></div>');
        }
    };
    
    Fisheye.prototype.render = function() {
        var isOneSlide = false;
        this.config.itemWidth = this.$element.find('.canvas').width() / 3;
        if(this.$element.find('.canvas').width() <= 640) {
            isOneSlide = true;
            this.config.itemWidth = this.$element.find('.canvas').width();
        }
        this.config.itemHeight = this.config.itemWidth * this.settings.ratio;
        this.config.carouselHeight = this.config.itemHeight * 1.5;
        this.config.marginTop = this.config.itemHeight / -2.0;
        this.config.activeItemWidth = this.config.itemWidth * this.config.activeItemScale;
        this.config.activeItemHeight = this.config.itemHeight * this.config.activeItemScale;
        if(isOneSlide) {
            this.config.activeItemWidth = this.config.itemWidth;
            this.config.activeItemHeight = this.config.itemHeight;
        }
        this.config.activeMarginTop = this.config.activeItemHeight / -2.0;
        this.config.activeMarginLeft = this.config.activeItemWidth / -2.0;
        
        this.$element.find('.canvas').css({
            height : this.config.carouselHeight
        });
        this.$element.find('.next, .prev').css({
            height : this.config.carouselHeight,
            lineHeight : this.config.carouselHeight
        });
        
        var activeIndex = Math.floor((this.config.itemCount - 1) / 2); // middle index
        if(this.$element.find('.item.active').length > 0) {
            activeIndex = parseInt(this.$element.find('.item.active').attr('data-index'));
        }
        
        this.navigate(activeIndex);
    };
    
    Fisheye.prototype.navigate = function(index) {
        if(index < 0 || index >= this.config.itemCount) return;
        
        this.$element.find('.active, .leftside, .rightside').removeClass('active leftside rightside');
        this.$element.find('.dot').eq(index).addClass('active');
        
        var $activeItem = this.$element.find('.item:eq(' + index + ')').addClass('active');
        var $leftItem = $activeItem.prev();
        var $rightItem = $activeItem.next();
        
        this.$element.find('.item').css({
            width : this.config.itemWidth,
            height : this.config.itemHeight,
            marginTop : this.config.marginTop,
            marginLeft : 0
        });
        
        $activeItem.css({
            width : this.config.activeItemWidth,
            height : this.config.activeItemHeight,
            marginTop : this.config.activeMarginTop,
            marginLeft : this.config.activeMarginLeft
        });
        
        $leftItem.addClass('leftside');
        $rightItem.addClass('rightside');
        
        var isOneSlide = false;
        if(this.$element.find('.canvas').width() <= 640) {
            isOneSlide = true;
        }
        
        var activeIndex = parseInt(this.$element.find('.item.active').attr('data-index'));
        var o = this;
        this.$element.find('.item').each(function() {
            if($(this).is('.active')) return;
            if(!isOneSlide) {
                if($(this).is('.leftside')) {
                    $(this).css({
                        left : 0,
                        right : 'auto'
                    });
                    return;
                }
                if($(this).is('.rightside')) {
                    $(this).css({
                        left : 'auto',
                        right : 0
                    });
                    return;
                }
            }
            
            var index = parseInt($(this).attr('data-index'));
            var leftPos = (index - activeIndex + 1) * o.config.itemWidth;
            if(isOneSlide) {
                leftPos = (index - activeIndex) * o.config.itemWidth;
            }
            
            $(this).css({
                left : leftPos,
                right : 'auto'
            });
        });
    };
    
    Fisheye.prototype.start = function() {
        if(this.settings.interval <= 0) return
        var o = this;
        this.slideTimer = setInterval(function() {
            var activeIndex = 0;
            if(o.$element.find('.item.active').length > 0) {
                activeIndex = parseInt(o.$element.find('.item.active').attr('data-index'));
                activeIndex++;
                if(activeIndex >= o.config.itemCount) {
                    activeIndex = 0;
                }
            }
            o.navigate(activeIndex);
        }, this.settings.interval);
    };
    
    Fisheye.prototype.stop = function() {
        if(this.slideTimer) {
            clearInterval(this.slideTimer);
            this.slideTimer = null;
        }
    };

})(jQuery);